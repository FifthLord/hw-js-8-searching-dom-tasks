
for (let p of document.body.getElementsByTagName("p")) {
   p.style.backgroundColor = "#ff0000";
}

console.log(document.getElementById("optionsList"));
console.log(document.getElementById("optionsList").parentElement);

for (let child of document.getElementById("optionsList").childNodes) {
   console.log(child.nodeType, child.nodeName);
}

//-----Розшифрував числові коди типів нод в текст-----//
for (let child of document.getElementById("optionsList").childNodes) {
   switch (child.nodeType) {
      case 1: console.log('ELEMENT_NODE'); break;
      case 2: console.log('ATTRIBUTE_NODE'); break;
      case 3: console.log('TEXT_NODE'); break;
      case 4: console.log('CDATA_SECTION_NODE'); break;
      case 5: console.log('ENTITY_REFERENCE_NODE'); break;
      case 6: console.log('ENTITY_NODE'); break;
      case 7: console.log('PROCESSING_INSTRUCTION_NODE'); break;
      case 8: console.log('COMMENT_NODE'); break;
      case 9: console.log('DOCUMENT_NOD'); break;
      case 10: console.log('DOCUMENT_TYPE_NODE'); break;
      case 11: console.log('DOCUMENT_FRAGMENT_NODE'); break;
      case 12: console.log('NOTATION_NODE'); break;
      default: break;
   }
};


let testA = document.getElementById('testParagraph');
testA.innerHTML = "<p>This is a paragraph</p>";


let testB = document.getElementsByClassName('main-header')[0];
for (let elem of testB.children) {
   console.log(elem);
   elem.classList.add("nav-item");
}

let testC = document.getElementsByClassName('section-title');
for (let elem of testC) {
   elem.classList.remove("section-title");
}